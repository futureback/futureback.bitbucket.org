module Main (main) where

import System.Environment
import FutureBack.Paul

main = do
     args <- getArgs
     prog <- getProgName
     run prog args
