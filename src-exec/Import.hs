module Main where

import FutureBack.Moses.Import
import System.Environment (getArgs, getProgName)
import System.IO.Unsafe (unsafePerformIO)

usage = "Usage: " ++ unsafePerformIO getProgName ++ " rdf"

-- |imports the rdf triples in the file which is provided at the command line 
-- into the futureBack db
main = do
     args <- getArgs
     case args of 
          [fp] -> runImport fp
          _ -> error usage
