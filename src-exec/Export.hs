module Main where

import FutureBack.Moses.Export
import System.Environment (getArgs, getProgName)
import System.IO.Unsafe (unsafePerformIO)

usage = "Usage: " ++ unsafePerformIO getProgName ++ " target-filename"

-- |exports the database into an rdf file (ntriples format)
main = do
     args <- getArgs
     case args of 
          [fp] -> runExport fp
          _ -> error usage
