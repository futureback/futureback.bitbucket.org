{-# LANGUAGE FlexibleContexts #-}

{- |
Module      : FutureBack.Paul.Restore
Copyright   : (c) Michael Hartl, Martin Stücklschwaiger 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : unix

The main module for restoring a backup.
-}

module FutureBack.Paul.Restore (restore) where

import Control.Monad.Error
import FutureBack.Common
import FutureBack.Moses.DBT
import FutureBack.Moses.FileStore
import FutureBack.Moses.Operations
import qualified Data.Text as T

-- | Restore a backup. 
-- TODO: remote path should be optional in the future, because we could
-- take the original path it was saved from as a default.
restore :: (MonadIO m, Functor m)
    => RawFilePath -- ^target path where the restored files shall appear
    -> RawFilePath -- ^source path where the backup was saved
    -> T.Text      -- ^Unique ID of the backup
    -> DBT m ()
restore local remote backupId = runHandlerT' $ do

  ensureDirectory local Nothing

  dirs <- lift $ getDirectories backupId
  files <- lift $ getFiles backupId

  forM dirs $ \(_, localFP, fileMeta) ->
       restoreDirectory (local // localFP) (metaPermissions fileMeta)

  forM files $ \i@(remoteName', localFP, fileMeta) -> do
      case remoteName' of
       Nothing -> throwError . strMsg $ "restore: Internal error: File without hash."
       Just remoteName -> do
           -- restoreFile :: RawFilePath -> RawFilePath -> FileMode -> EpochTime
           restoreFile (remote /+ remoteName)
                       (local // localFP)
                       (metaPermissions fileMeta)
                       (metaModified fileMeta)

  return ()

