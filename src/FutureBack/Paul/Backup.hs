{-# LANGUAGE FlexibleContexts
  , RankNTypes
  , ScopedTypeVariables #-}

{- |
Module      : FutureBack.Paul.Backup
Copyright   : (c) Michael Hartl, Martin Stücklschwaiger 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : unix

The main module for executing a backup.
-}

module FutureBack.Paul.Backup (backup) where

import FutureBack.Common
import FutureBack.Paul.DirWalk
import FutureBack.Moses.Operations
import FutureBack.Moses.BackupStore
import FutureBack.Moses.FileStore
import FutureBack.Moses.DBT
import Pipes
import qualified Data.Text as T
import qualified Data.ByteString.Char8 as B

processFiles :: forall m. (MonadIO m, Functor m) => 
                Backup -> RawFilePath -> RawFilePath -> HandlerT (DBT m) ()
processFiles backup source destination = runEffect . for (findFiles source) $ lift . process

    where 
      process :: Either ErrorMsg FileInfo -> HandlerT (DBT m) ()
      process (Left err) = lift . throwError $ err
      process (Right fi@(fp, _)) = do
              output fp
              fih <- lift $ infoToInfoHash source fi
              lift $ saveFileInfoHash backup fih
              case mkTargetName fih of
                   Just tn -> saveFile fp destination tn
                   Nothing -> return ()

      output :: RawFilePath -> HandlerT (DBT m) ()
      output fp = lift . liftIO . B.putStrLn $ fp

      saveFile :: RawFilePath -> RawFilePath -> T.Text -> HandlerT (DBT m) ()
      saveFile fp destination tn = backupFileAs (source // fp) tn destination

-- | Run backup.
backup :: forall m. (MonadIO m, Functor m)
       => RawFilePath  -- ^Source path to be backed up
       -> RawFilePath  -- ^Target path to store the backup in
       -> DBT m ()
backup source destination = runHandlerT' go
  where go :: HandlerT (DBT m) ()
        go = do
          ensureDirectory destination Nothing
          backup <- lift . liftIO $ createBackup source destination
          lift $ saveBackupStarted backup
          processFiles backup source destination
          lift $ saveBackupDone backup
          return ()
