{-# LANGUAGE OverloadedStrings,
             DoAndIfThenElse,
             FlexibleContexts #-}

{- |
Module      : FutureBack.Paul.DirWalk
Copyright   : (c) Michael Hartl, Martin Stücklschwaiger 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : unix

Find filesystem objects to be backed up.
-}

module FutureBack.Paul.DirWalk (findFiles) where

import System.Posix.Directory.ByteString
import System.Posix.Files.ByteString (getSymbolicLinkStatus)
import FutureBack.Common
import Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString.Lazy as L
import Pipes

-- quick and dirty fix to assure that our relative paths
-- don't start with a slash.
(</>) :: ByteString -> ByteString -> ByteString
d </> f | d == "/" = B.append "/" f
        | otherwise = B.concat [d, "/", f]

-- | Walk a directory and yield 'FileInfo' for every file system item inside.
--   Symlinks are ignored.
findFiles :: (MonadIO m, MonadError ErrorMsg m) =>
           ByteString -> Producer (Either ErrorMsg FileInfo)  m ()
findFiles root = go ""
        where
          go top = do
            ds <- errIO $ openDirStream (root <//> top)
            enum top ds
          enum top ds = do fp <- tryIO $ readDirStream ds
                           case fp of
                            Left err -> continue top ds (yield $ Left err)
                            Right fp' -> do
                               if fp' == B.empty
                               then tryIO_ $ closeDirStream ds
                               else if fp' `elem` [".", ".."]
                                    then continue top ds (return ())
                                    else continue top ds (handleFile $ top <//> fp')

          handleFile fp = do
              stat <- tryIO $ getSymbolicLinkStatus (root <//> fp)
              case stat of
                   Left err -> yield (Left err)
                   Right stat' -> do
                         yield $ Right (fp, stat')
                         if stat' `isFileType` Directory
                         then go fp
                         else return ()

          continue top ds rec = rec >> enum top ds

          "" <//> y = y
          x  <//> y = x </> y

