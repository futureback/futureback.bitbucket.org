{-# LANGUAGE FlexibleContexts #-}

{-| 
Module      : FutureBack.Common.Util
Copyright   : (c) Michael Hartl, Martin Stücklschwaiger, 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : portable

Some assorted utility functions
-}

module FutureBack.Common.Util 
       ( tryIO
       , tryIO_
       , errIO
       , isFileType
       , (//)
       , (/+)
       , (|+)
       , baseName
       , mkTargetName
       , createUniqueId
) where

import System.IO.Error
import Control.Monad.IO.Class
import FutureBack.Common.Types
import FutureBack.Common.Convert
import System.FilePath.Posix ((</>), takeBaseName)
import qualified Data.ByteString.Char8 as B
import qualified Data.Text as T
import qualified Data.Text.Encoding as E
import qualified System.Random as R
import System.Posix.Time (epochTime)

-- | try some IO action and return the result as an 'Either',
-- wrapping any error in 'FutureBack.Common.Types.IOError'
tryIO :: (MonadIO m) => IO a -> m (Either ErrorMsg a)
tryIO f = do res <- liftIO $ tryIOError f
             case res of
                  Left err -> return . Left . IOError $ err
                  Right res' -> return . Right $ res'

-- | like 'tryIO', but ignore result
tryIO_ :: (MonadIO m) => IO a -> m ()
tryIO_ f = tryIO f >> return ()

-- | like 'tryIO', but throw the error in 'MonadError' instead 
-- of returning it.
errIO :: (MonadIO m, MonadError ErrorMsg m) => IO a -> m a
errIO f = do 
      res <- tryIO f
      case res of
           Left err -> throwError err
           Right res' -> return res'

-- | See if a file has a specific 'FileType' according to its 'FileStatus'
isFileType :: FileStatus -> FileType -> Bool
isFileType stat tp = fileTypeFromFileStatus stat == tp

-- | Concatenate 'RawFilePath's, ensuring there is a single separator
-- in between. This uses 'System.FilePath.Posix.(\</\>)' internally
(//) :: RawFilePath -> RawFilePath -> RawFilePath
a // b = B.pack $ B.unpack a </> B.unpack b

-- | Like '(//)', but append 'T.Text'
(/+) :: RawFilePath -> T.Text -> RawFilePath
a /+ b = a // rawFilePathFromText b

-- | Append 'T.Text' to a 'RawFilePath' without a separator in between.
(|+) :: RawFilePath -> T.Text -> RawFilePath
a |+ b = a `B.append` (E.encodeUtf8 b)

-- | Get the base name of a file. This uses 'System.FilePath.Posix.takeBaseName' 
-- internally.
baseName :: RawFilePath -> RawFilePath
baseName = B.pack . takeBaseName . B.unpack

-- | Create a random 'UniqueId'
createUniqueId :: IO UniqueId
createUniqueId = do
    let bounds = strLenToRandLen 16
    rand <- R.randomRIO bounds
    let str = integerToNiceStr $ abs rand
    return . T.pack $ str
  where
    strLenToRandLen i = (62^(i-1)-1, 62^i-1)

-- | Determine the file name a backuped file should have on the target medium. 
-- TODO: Right now, the file will probably be restored incorrectly if this 
-- is changed to use something other than the hash. Store the target name and 
-- use it for restoring!
mkTargetName :: FileInfoHash -> Maybe T.Text
mkTargetName (_,_,Just h)  = Just h
mkTargetName (_,_,Nothing) = Nothing
