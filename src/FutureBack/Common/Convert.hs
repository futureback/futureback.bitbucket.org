{-# LANGUAGE OverloadedStrings #-}

{-| 
Module      : FutureBack.Common.Convert
Copyright   : (c) Martin Stücklschwaiger, Michael Hartl, 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : portable

Utility functions to convert between datatypes commonly used in the program. Also to
convert between certain data to RDF 'Node's and back.
-}

module FutureBack.Common.Convert 
    ( filePathToNode
    , nodeToFilePath
    , fileDateToNode
    , nodeToFileDate
    , fileMetaToNode
    , nodeToFileMeta
    , fileTypeToNode
    , fileHashToNode
    , backupToNode
    , fileMetaFromFileStatus
    , fileSizeFromFileStatus
    , fileTypeFromFileStatus
    , rawFilePathToFilePath
    , rawFilePathToText
    , rawFilePathFromText
    , md5HashToNiceHash
    , nodeToNiceHash
    , integerToNiceStr
    ) where

import System.Posix.Files.ByteString
import System.Posix.ByteString.FilePath
import FutureBack.Common.Types
import Data.RDF
import Control.Arrow (right)
import Text.Read (readEither)
import qualified Data.Text as T
import qualified Data.Text.Encoding as E
import qualified Data.ByteString.Char8 as B
import qualified Numeric as Numeric
import Data.Digest.Pure.MD5 (MD5Digest)

-- | Convert a 'RawFilePath' to a literal RDF 'Node'
filePathToNode :: RawFilePath -> Node
filePathToNode rfp = lnode . plainL . E.decodeUtf8 $ rfp

-- | Extract a 'RawFilePath' from a literal RDF 'Node'
nodeToFilePath :: Node -> Either ErrorMsg RawFilePath
nodeToFilePath n = right E.encodeUtf8 . fromLiteral $ n

-- | Convert a 'FileDate' to a literal RDF 'Node'
fileDateToNode :: FileDate -> Node
fileDateToNode = showToNode

-- | Extract a 'FileDate' from a literal RDF 'Node'
nodeToFileDate :: Node -> Either ErrorMsg FileDate
nodeToFileDate n = do t <- fromLiteral n
                      case readEither . T.unpack $ t of
                           Left str -> Left . strMsg $ str
                           Right d -> Right d

-- | Convert a 'FileMeta' record to a literal RDF 'Node'
fileMetaToNode :: FileMeta -> Node
fileMetaToNode = showToNode

-- | Extract a 'FileMeta' record form a literal RDF 'Node'
nodeToFileMeta :: Node -> Either ErrorMsg FileMeta
nodeToFileMeta n = do t <- fromLiteral n
                      case readEither . T.unpack $ t of
                          Left str -> Left . strMsg $ str
                          Right d -> Right d

-- | Convert a 'FileType' to a literal RDF 'Node'
fileTypeToNode :: FileType -> Node
fileTypeToNode = showToNode

-- | Convert a 'NiceHash' to a literal RDF 'Node'
fileHashToNode :: NiceHash -> Node
fileHashToNode = LNode . PlainL

-- | Extract a 'NiceHash' from a literal RDF 'Node'
nodeToNiceHash :: Node -> Either ErrorMsg NiceHash
nodeToNiceHash = fromLiteral

-- | Create a 'Node' corresponding to a 'Backup'
backupToNode :: Backup -> Node
backupToNode = LNode . PlainL . backupId

-- Convert anything showable to a node
showToNode :: (Show a) => a -> Node
showToNode = LNode . PlainL . T.pack . show

-- Convert a literal node to text
fromLiteral :: Node -> Either ErrorMsg T.Text
fromLiteral (LNode (PlainL t)) = Right t
fromLiteral n = Left . strMsg $ "fromLiteral: unexpected node type " ++ show n

-- | Creates a 'FileMeta' out of 'FileStatus'
fileMetaFromFileStatus :: FileStatus -> FileMeta
fileMetaFromFileStatus fstat = FileMeta
    { metaPermissions = fileMode fstat
    , metaOwner = (fileOwner fstat, fileGroup fstat)
    , metaFilesize = fileSizeFromFileStatus fstat
    , metaModified = modificationTime fstat
    , metaFileType = fileTypeFromFileStatus fstat
    }

--  | Gets the file size from a 'FileStatus'
fileSizeFromFileStatus :: FileStatus -> FileOffset
fileSizeFromFileStatus fstat
  | isRegularFile fstat = fileSize fstat
  | otherwise           = 0

-- | Gets the 'FileType' from a 'FileStatus'
fileTypeFromFileStatus :: FileStatus -> FileType
fileTypeFromFileStatus fstat
  | isRegularFile     fstat = RegularFile
  | isDirectory       fstat = Directory
  | isSymbolicLink    fstat = Symlink
  | isBlockDevice     fstat = BlockDevice
  | isCharacterDevice fstat = CharDevice
  | otherwise               = UnknownFileType

-- | Convert a 'RawFilePath' to a standard 'FilePath'
rawFilePathToFilePath :: RawFilePath -> FilePath
rawFilePathToFilePath = B.unpack

-- | Convert a 'RawFilePath' to 'T.Text'
rawFilePathToText :: RawFilePath -> T.Text
rawFilePathToText = E.decodeUtf8

-- | Make a 'RawFilePath' out of 'T.Text'
rawFilePathFromText :: T.Text -> RawFilePath
rawFilePathFromText = E.encodeUtf8

-- | Convert a 'MD5Digest' to 'T.Text'
md5HashToNiceHash :: MD5Digest -> NiceHash
md5HashToNiceHash = T.pack . integerToNiceStr . hashToInteger

-- | Convert a 'MD5Digest' to a number
hashToInteger :: MD5Digest -> Integer
hashToInteger = hexToInteger . show

hexToInteger :: String -> Integer
hexToInteger = fst . head . Numeric.readHex

-- | Show an integer nicely
-- TODO why is 'printf' or 'show' not sufficient here?
integerToNiceStr :: Integer -> String
integerToNiceStr int
    | int < 0   = error "integerToNiceStr: value is < 0"
    | int < len = [char]
    | otherwise = char : rest
  where
    char = vals `B.index` (fromIntegral $ int `mod` len)
    rest = integerToNiceStr (int `div` len)
    len  = toInteger . B.length $ vals
    vals = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
