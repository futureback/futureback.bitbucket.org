{-# LANGUAGE FlexibleContexts #-}

{-|
Module      : FutureBack.Common.Conditions
Copyright   : (c) Michael Hartl, 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : portable

This module implements a simple condition system inspired by the one found
in common lisp. Conditions are just a type of error and as such, a data constructor
to 'ErrMsg'. However, conditions are associated with one or more available 'BackupRestart's, 
and when a condition is thrown inside 'ErrorT', one of a bunch of 
handlers is applied to select a restart. By default, the user is asked. When a restart
is found, the original action may continue where it left of. The stack is not unwound 
unless no handler handled the condition.
-}

module FutureBack.Common.Conditions 
    ( BackupCondition(..)
    , BackupRestart(..)
    , RestartCondition(..)
    , handleConditions
    , RestartHandler
    , askUser
    , HandlerT
    , runHandlerT
    , runHandlerT'
    ) where

import FutureBack.Common.Types
import Text.Read (readMaybe)
import Control.Monad.State
import Debug.Trace

-- |Run a condition-enabled action, handling conditions on the way.
handleConditions :: (MonadIO m, MonadError ErrorMsg m) 
                 => (BackupRestart -> HandlerT m a) -- ^The continuation to run after 
                                                    -- a restart is selected
                 -> HandlerT m a                    -- ^The action to run
                 -> HandlerT m a
handleConditions useRestart f = catchError f $ \e ->
    case e of
      BackupError cond -> do
             handlers <- get
             tryHandlers e handlers
      _ -> throwError e
  where tryHandlers e [] = throwError e
        tryHandlers e@(BackupError cond) (handler:hs) = do
             res <- lift $ handler cond
             case res of
               Nothing -> tryHandlers e hs
               Just restart -> case restart of
                    OverwriteAll -> add cond Overwrite >> useRestart Overwrite
                    Overwrite    ->                       useRestart Overwrite
                    MergeAll     -> add cond Merge     >> useRestart Merge
                    Merge        ->                       useRestart Merge
                    Abort        ->                       useRestart Abort
                    Retry        -> handleConditions useRestart f
        add (RestartCondition conds _) r = addHandler $ \(RestartCondition conds' _) ->
            if map conditionType conds' == map conditionType conds
            then return $ Just r
            else return Nothing

-- | Default handler for command line programs. Prints conditions and
-- available restarts until the user selects a valid restart.
askUser :: (MonadIO m) => RestartHandler m
askUser c@(RestartCondition conds restarts) = liftIO $ do
    putStrLn "\nThe following conditions occurred"
    mapM_ printCond conds
    putStrLn "Available restarts:"
    mapM_ printRestart (zip [1..] restarts)
    readRestart
  where printCond = print
        printRestart :: (Int, BackupRestart) -> IO ()
        printRestart (i, r) = putStrLn $ "[" ++ show i ++ "] " ++ show r
        readRestart = do restart <- readLoopStdin :: IO Int
                         if   restart <= length restarts && restart >= 0
                         then return $ Just (restarts !! (restart - 1))
                         else do print $ "Restart '" ++ show restart ++ "' not available"
                                 askUser c

-- |An action with restart handlers which may change over the course 
-- of the program.
type HandlerT m = StateT [RestartHandler m] m

-- |Run a condition-enabled action.
runHandlerT :: (Monad m) 
            => HandlerT m a       -- ^the action to run
            -> [RestartHandler m] -- ^a list of initial handlers
            -> m a
runHandlerT = evalStateT

-- |like 'runHandlerT', but with 'askUser' as the single initial handler.
runHandlerT' :: (MonadIO m) => HandlerT m a -> m a
runHandlerT' = flip runHandlerT [askUser]

addHandler :: (Monad m) => RestartHandler m -> HandlerT m ()
addHandler h = get >>= put . (h:)

-- When two BackupConditions have the same conditionType, then 
-- they are considered the same for the purpose of not asking
-- the user again after he chose an -All restart and the same
-- condition occurs. Current policy is to ignore file paths, 
-- but tread different file types as different, so that e.g. 
-- overwriting all files is not the same as overwriting all
-- directories.
conditionType :: BackupCondition -> String
conditionType (FileExists tp _) = show tp ++ "Exists"
conditionType (WrongFileType _ _ _) = "WrongFileType"
conditionType (WrongFileMode _ _ _) = "WrongFileMode"

-- try to read some value from stdin until it is read correctly
readLoopStdin :: (Read a, MonadIO m) => m a
readLoopStdin = do res <- liftIO getLine
                   case readMaybe res of
                     Nothing -> readLoopStdin
                     Just x -> return x

