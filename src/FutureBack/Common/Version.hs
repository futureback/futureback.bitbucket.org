{-| 
Module      : FutureBack.Common.Version
Copyright   : (c) Michael Hartl, 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : portable

This module stores just the program version, so we can output it at the
command line.
-}

module FutureBack.Common.Version (fbVersion) where

-- | The program version. If you change this, change it in the cabal file as well!
fbVersion :: String
fbVersion = "0.0.0.1"
