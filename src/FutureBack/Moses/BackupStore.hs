{-# LANGUAGE
    RankNTypes #-}

{- |
Module      : FutureBack.Moses.BackupStore
Copyright   : (c) Martin Stücklschwaiger 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : portable

Triple-Store for backups.
-}
module FutureBack.Moses.BackupStore 
    ( createBackup
    , saveBackupStarted
    , saveBackupDone
    , getBackupTimes
    ) where

import FutureBack.Moses.Store
import FutureBack.Moses.Vocabulary
import FutureBack.Common
import Data.RDF
import qualified Data.Text as T
import System.Posix.Time (epochTime)

-- | Create a new backup node
createBackup 
    :: RawFilePath -- ^Source directory
    -> RawFilePath -- ^Destination directory
    -> IO Backup
createBackup source dest = do
  uid <- createUniqueId
  now <- epochTime
  return Backup
    { backupId          = uid
    , backupStartTime   = now
    , backupSource      = source
    , backupDestination = dest
    , mBackupEndTime    = Nothing
    }

-- | Saves a backup-node with additional infos
--   Does not store backupEndTime (because time of end is not known yet)
saveBackupStarted :: Backup -> DbOp ()
saveBackupStarted backup = do
  let triples = backupToTriples backup
  _ <- insertTriples triples
  return ()

-- | Updates only the backupEndTime in the DB
--   Returns an updated backup.
--   TODO (martin): this should somehow be decoupled
saveBackupDone :: Backup -> DbOp Backup
saveBackupDone backup = do
    now <- liftIO epochTime
    let backupNode = backupToNode backup
    let backupEndNode = fileDateToNode now
    insertTriple $ Triple backupNode fbEndTime backupEndNode
    let newBackup = backup { mBackupEndTime = Just now }
    return newBackup

-- | List all the backups which have been registered and finished
-- so far.
getBackupTimes :: DbOp [(BackupId, Maybe EpochTime)]
getBackupTimes = do
    triples <- triplesByPredicate fbEndTime
    let backupNodes = map subjectOf triples
    let backupIds = map extractValue backupNodes
    backupTimes <- mapM findTime backupNodes
    let backups = zip backupIds backupTimes
    return backups
  where
    extractValue :: Node -> T.Text
    extractValue (LNode(PlainL(value))) = value
    extractValue _                      = error "getBackupTimes: invalid Node type"

    findTime :: Subject -> DbOp (Maybe EpochTime)
    findTime backupNode = do
      triples <- triplesBySubjectAndPredicate backupNode fbStartTime
      
      case triples of
        []             -> return Nothing
        (timeTriple:_) -> do
            let timeNode = objectOf timeTriple
            let timeValue = extractValue timeNode
            return $ Just (read $ T.unpack timeValue)


-- | Returns all needed triples, except for backupEndTime
--   TODO (martin): Add backupEndTime
backupToTriples :: Backup -> Triples
backupToTriples backup = 
    [ Triple backupNode fbStartTime   startNode
    , Triple backupNode fbSource      sourceNode
    , Triple backupNode fbDestination destNode
    ]
  where
    backupNode = backupToNode backup
    startNode  = fileDateToNode $ backupStartTime backup
    sourceNode = filePathToNode $ backupSource backup
    destNode   = filePathToNode $ backupDestination backup
