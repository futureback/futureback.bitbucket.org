{- |
Module      : FutureBack.Moses.Operations
Copyright   : (c) Michael Hartl 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : unix

File system operations used by futureBack.
-}

{-# LANGUAGE OverloadedStrings, FlexibleContexts, RankNTypes #-}
module FutureBack.Moses.Operations
       ( Op
       , backupFileAs
       , restoreFile
       , restoreDirectory
       , ensureDirectory
       , defaultFilePermissions
       , defaultDirPermissions
       ) where

import FutureBack.Common

import qualified Data.Text as T
import qualified Data.ByteString.Char8 as B
import GHC.IO.Exception (ioe_type, IOErrorType(NoSuchThing))
import qualified System.Posix.Files.ByteString     as P
import qualified System.Posix.Directory.ByteString as P
import qualified System.Directory as Dir
import qualified System.IO as IO
import qualified System.IO.Error as IOE
import qualified Control.Exception.Base as E
import System.FilePath.Posix (takeDirectory)
import Foreign.Marshal.Alloc (allocaBytes)


-- | default permissions to use for a file
defaultFilePermissions :: FileMode
defaultFilePermissions = P.stdFileMode

-- | default permissions to use for a directory
defaultDirPermissions :: FileMode
defaultDirPermissions = P.accessModes

-- | conveniency synonym for a condition-handled operation
type Op a = forall m . (MonadIO m, MonadError ErrorMsg m) => HandlerT m a

checkExisting :: RawFilePath -> Op (Maybe (FileType, FileMode))
checkExisting fp = do
    stat <- tryIO $ P.getSymbolicLinkStatus fp
    case stat of
      Left (IOError err) 
                  -> if   ioe_type err == NoSuchThing
                     then return Nothing
                     else throwError (IOError err)
      Left err    -> throwError err
      Right fstat -> return . Just $ (fileTypeFromFileStatus fstat, P.fileMode fstat)

throwIfExists :: RawFilePath ->
                 FileType ->
                 FileMode ->
                 [BackupRestart] ->
                 Op ()
throwIfExists fp ft fm restarts = do
    res <- checkExisting fp
    case res of
      Nothing             -> return ()
      Just (ftype, fmode) ->
          let conds   = [FileExists ft fp]
              conds'  = if ft /= ftype
                        then conds ++ [WrongFileType fp ft ftype]
                        else conds
              conds'' = if fm /= fmode
                        then conds' ++ [WrongFileMode fp fm fmode]
                        else conds'
          in throwError (BackupError $ RestartCondition conds'' restarts)

handleExisting :: RawFilePath ->
                  FileType ->
                  FileMode ->
                  (BackupRestart -> Op ()) ->
                  [BackupRestart] ->
                  Op ()
handleExisting fp ftype fmode useRestart restarts = 
  handleConditions useRestart $ throwIfExists fp ftype fmode restarts

-- | remove a file
removeFile :: RawFilePath -> Op ()
removeFile fp = errIO $ P.removeLink fp

-- | Remove a file system object, regardless of type. Directories are deleted recursively.
removeAnything :: RawFilePath -> Op ()
removeAnything tgt = do res <- tryIO $ P.getSymbolicLinkStatus tgt
                        case res of
                             Right fstat -> case fileTypeFromFileStatus fstat of
                                                 Directory -> recursiveDelete tgt fstat
                                                 _ -> liftIO $ P.removeLink tgt
                             Left _ -> return ()

-- | Remove an empty directory.
removeDirectory :: RawFilePath -> Op ()
removeDirectory fp = errIO $ P.removeDirectory fp

-- | Copy a file.
--   Also copy the permissions from the source.
copyFile 
    :: RawFilePath -- ^Source
    -> RawFilePath -- ^Destination
    -> Op ()
copyFile src tgt = errIO $ Dir.copyFile (rawFilePathToFilePath src) (rawFilePathToFilePath tgt)


-- | Copies a file but does not touch permissions.
copyFileWithoutPermissions 
    :: RawFilePath -- ^Source
    -> RawFilePath -- ^Destination
    -> Op ()
copyFileWithoutPermissions src tgt = errIO $ 
    copyFileWithoutPermissions' (rawFilePathToFilePath src) (rawFilePathToFilePath tgt)


-- This is a verbatim copy of 'System.Directory.copyFile' with just the copyPermissions
-- part removed.
copyFileWithoutPermissions' :: FilePath -> FilePath -> IO ()
copyFileWithoutPermissions' fromFPath toFPath = 
    copy `IOE.catchIOError` (\exc -> E.throw $ IOE.ioeSetLocation exc "copyFile")
    where copy = E.bracket (IO.openBinaryFile fromFPath IO.ReadMode) IO.hClose $ \hFrom ->
                 E.bracketOnError openTmp cleanTmp $ \(tmpFPath, hTmp) ->
                 do allocaBytes bufferSize $ copyContents hFrom hTmp
                    IO.hClose hTmp
                    Dir.renameFile tmpFPath toFPath
          openTmp = IO.openBinaryTempFile (takeDirectory toFPath) ".copyFile.tmp"
          cleanTmp (tmpFPath, hTmp)
              = do ignoreIOExceptions $ IO.hClose hTmp
                   ignoreIOExceptions $ Dir.removeFile tmpFPath
          bufferSize = 1024

          copyContents hFrom hTo buffer = do
                  count <- IO.hGetBuf hFrom buffer bufferSize
                  when (count > 0) $ do
                          IO.hPutBuf hTo buffer count
                          copyContents hFrom hTo buffer

          ignoreIOExceptions io = io `IOE.catchIOError` (\_ -> return ())


-- | Create a new directory.
createDirectory :: RawFilePath -> FileMode -> Op ()
createDirectory fp perm = errIO $ P.createDirectory fp perm

-- | backup a single file
backupFileAs
    :: RawFilePath  -- ^the original file
    -> T.Text       -- ^the file name it should have at the target
    -> RawFilePath  -- ^the target directory where it should be stored
    -> Op ()
backupFileAs fp targetName targetPath = copyFileWithoutPermissions fp (targetPath /+ targetName)

-- | restore a single file
restoreFile
    :: RawFilePath   -- ^path of the file in the backup
    -> RawFilePath   -- ^path where it shall be restored to
    -> FileMode      -- ^permissions
    -> EpochTime     -- ^modification time (ignored for now)
    -> Op ()
restoreFile src tgt perm modDate = do 
            handleExisting tgt RegularFile perm delOrAbort
                               [Retry, Abort, Overwrite, OverwriteAll]
            copyFile src tgt
            errIO $ P.setFileMode tgt perm
            -- TODO: set mod time
  where delOrAbort Abort     = throwError . strMsg $ "aborted by user"
        delOrAbort Overwrite = removeAnything tgt

-- | Restore a single directory. If it exists, then it will be deleted or
--   overwritten, depending on the condition handlers and potentially user input.
restoreDirectory :: RawFilePath -> FileMode -> Op ()
restoreDirectory tgt perm = do
              handleExisting tgt Directory perm doRestart [Retry, Abort, Overwrite, OverwriteAll,
                                                           Merge, MergeAll]
              stat <- tryIO $ P.getSymbolicLinkStatus tgt
              case stat of
                   Right _ -> do recursiveDeleteContents tgt
                                 removeDirectory tgt
                   _ -> return ()
              createDirectory tgt perm
    where doRestart Abort     = throwError . strMsg $ "aborted by user"
          doRestart Overwrite = do
             removeAnything tgt

          doRestart Merge     = return ()

-- | ensure a directory exists and has the correct permissions
ensureDirectory :: RawFilePath -> Maybe FileMode -> Op ()
ensureDirectory fp perm = do
              res <- tryIO $ P.getSymbolicLinkStatus fp
              case res of
                   Left _ -> createDirectory fp perm'
                   Right fstat -> if   fstat `isFileType` Directory
                                  then adjustPerms fstat
                                  else throwError . strMsg $ B.unpack fp
                                            ++ " already exists, but is not a directory."
    where perm' = case perm of
                       Nothing -> defaultDirPermissions
                       Just p -> p
          adjustPerms fstat = 
                  case perm of
                       Nothing -> return ()
                       Just p -> when (P.fileMode fstat /= p) $
                                     errIO (P.setFileMode fp p)

recursiveDelete :: RawFilePath -> FileStatus -> Op ()
recursiveDelete fp fs =
                case metaFileType . fileMetaFromFileStatus $ fs of
                     RegularFile     -> removeFile fp
                     Directory       -> do recursiveDeleteContents fp
                                           removeDirectory fp
                     Symlink         -> removeFile fp
                     BlockDevice     -> removeFile fp
                     CharDevice      -> removeFile fp
                     UnknownFileType -> removeFile fp

-- argument must be a directory
recursiveDeleteContents :: RawFilePath -> Op ()
recursiveDeleteContents start = do
              ds <- errIO $ P.openDirStream start
              go start ds

    where go base ds = do
             fp <- tryIO $ P.readDirStream ds
             case fp of 
                  Left _ -> return ()
                  Right fp' -> 
                    if fp' == "" 
                      then return ()
                      else do
                        processFp fp' base ds
                        go base ds

          processFp fp base ds | fp `elem` [".", ".."] = go base ds
                               | otherwise = do
                                           stat <- tryIO $ P.getSymbolicLinkStatus (base // fp)
                                           case stat of 
                                                Left err -> throwError (strMsg . show $ err)
                                                Right stat' -> do
                                                      recursiveDelete (base // fp) stat'
