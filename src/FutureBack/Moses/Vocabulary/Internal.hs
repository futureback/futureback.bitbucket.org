{-# LANGUAGE OverloadedStrings #-}

{- |
Module      : FutureBack.Moses.Vocabulary.Files
Copyright   : (c) Michael Hartl, Martin Stücklschwaiger 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : portable

Internal helpers for constructing vocabulary.
-}

module FutureBack.Moses.Vocabulary.Internal (
       module Data.RDF,
       commonPrefix,
       baseNode,
       (<//>), (<///>)) where

import Data.RDF
import qualified Data.Text as T

-- | our URL prefix
commonPrefix :: T.Text
commonPrefix = "http://futureback.io/filestore/"

-- | make a base node with the common prefix
baseNode :: Node
baseNode = UNode commonPrefix

-- | concatenate text urls separated by '/'
(<//>) :: T.Text -> T.Text -> T.Text
base <//> url = T.dropWhileEnd (== '/') base `T.append` 
                "/" `T.append` 
                T.dropWhile (== '/') url

-- | concatenate URL nodes
(<///>) :: Node -> T.Text -> Node
(UNode u) <///>  t = UNode (u <//> t)
_ <///> _ = error "Unexpected node types"
