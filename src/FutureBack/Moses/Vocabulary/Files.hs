{-# LANGUAGE OverloadedStrings #-}

{- |
Module      : FutureBack.Moses.Vocabulary.Files
Copyright   : (c) Michael Hartl, Martin Stücklschwaiger 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : portable

RDF vocabulary used by futureBack regarding files.
-}

module FutureBack.Moses.Vocabulary.Files 
  ( fbSeen
  , fbModified
  , fbMeta
  , fbHash
  , fbPath
  , fbType
  ) where


import FutureBack.Moses.Vocabulary.Internal

-- |
prefix :: Node
prefix = baseNode <///> "Files"

fbSeen :: Predicate
fbSeen = prefix <///> "seen"

fbModified :: Predicate
fbModified = prefix <///> "modified"

fbMeta :: Predicate
fbMeta = prefix <///> "meta"

fbPath :: Predicate
fbPath = prefix <///> "path"

fbType :: Predicate
fbType = prefix <///> "type"

fbHash :: Predicate
fbHash = prefix <///> "hash"

