{-# LANGUAGE OverloadedStrings #-}

{- |
Module      : FutureBack.Moses.Vocabulary.Backup
Copyright   : (c) Michael Hartl, Martin Stücklschwaiger 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : portable

RDF vocabulary used by futureBack regarding backups.
-}

module FutureBack.Moses.Vocabulary.Backup 
  ( theBackend
  , fbSource
  , fbDestination
  , fbHasFile
  , fbStartTime
  , fbEndTime
  ) where

import FutureBack.Moses.Vocabulary.Internal

-- |
prefix :: Node
prefix = baseNode <///> "backup"

fbStartTime :: Predicate
fbStartTime = prefix <///> "time"

fbSource :: Predicate
fbSource = prefix <///> "source"

fbDestination :: Predicate
fbDestination = prefix <///> "destination"

fbHasFile :: Predicate
fbHasFile = prefix <///> "hasFile"

fbEndTime :: Predicate
fbEndTime = prefix <///> "done"

theBackend :: Subject
theBackend = prefix <///> "theBackend"
