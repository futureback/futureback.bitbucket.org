{- |
Module      : FutureBack.Moses.Import
Copyright   : (c) Michael Hartl 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : unix

Main function for the import utility
-}

module FutureBack.Moses.Import (runImport) where

import Pipes
import FutureBack.Moses.Store

import Data.RDF
import Data.RDF.TriplesGraph
import Data.Either
import Data.Text (Text)
import qualified Data.Text as T

myBaseUrl = Nothing
mySelfRef = Nothing

-- | import an rdf file in any known format (NTriples, Turtle, XML)
runImport fp = do res <- tryParse fp myBaseUrl mySelfRef
                  case res of 
                       Nothing -> error $ "Cannot parse " ++ fp
                       Just graph -> runDb $ do
                                       createDb
                                       runEffect $ (each $ triplesOf graph) >-> insertTriplesP
                                       commit

tryParseNTriples, tryParseTurtle, tryParseXml :: 
                  FilePath -> Maybe BaseUrl -> Maybe Text -> IO (Maybe TriplesGraph)
tryParse fp bu sr = tryAll [tryParseNTriples, tryParseTurtle, tryParseXml]
         where tryAll []     = return Nothing
               tryAll (f:fs) = do res <- f fp bu sr
                                  case res of 
                                       Nothing -> tryAll fs
                                       Just rdf -> return $ Just rdf
tryParseNTriples fp _ _ = do res <- parseFile NTriplesParser fp
                             return $ either (const Nothing) Just res
tryParseTurtle fp bu sr = do res <- parseFile (TurtleParser bu sr) fp
                             return $ either (const Nothing) Just res
tryParseXml fp bu sr    = do res <- parseFile (XmlParser bu sr) fp
                             return $ either (const Nothing) Just res
