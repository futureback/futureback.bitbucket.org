{-# LANGUAGE NoMonomorphismRestriction,
             FlexibleContexts,
             ScopedTypeVariables,
             RankNTypes
 #-}

{- |
Module      : FutureBack.Moses.Store
Copyright   : (c) Michael Hartl, Martin Stücklschwaiger 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : unix

A simple triple store.
-}

module FutureBack.Moses.Store (module FutureBack.Moses.DBT,
                               DbOp,
                               getDbName,
                               runDb,
                               createTripleTable,
                               createDb,
                               dropDb,
                               resetDb,
                               readTriples,
                               readTriplesP,
                               insertTriples,
                               insertTriplesP,
                               insertTriple,
                               triplesBySubject,
                               triplesByObject,
                               triplesByPredicate,
                               triplesBySubjectAndPredicate,
                               triplesByPredicateAndObject,
                               triplesBySubjectAndObject
                               ) where

import Data.RDF
import Data.List (intercalate)
import Data.Convertible (Convertible) 
import FutureBack.Moses.DBT
import Control.Monad
import Control.Monad.IO.Class
import Control.Applicative
import Control.Monad.Error
import Pipes
import System.Directory (getAppUserDataDirectory, doesDirectoryExist, createDirectory)
import System.FilePath ((</>))
import FutureBack.Common

type DbOp a = forall m. (MonadIO m, Functor m) => DBT m a

-- |Our standard database filename.
getDbName :: IO String
getDbName = do
          d <- getAppUserDataDirectory "futureBack"
          e <- doesDirectoryExist d
          when (not e) $ createDirectory d
          return $ d </> "futureBack.db"

-- |Run a DB action on the standard database file
runDb :: (MonadIO m, Functor m) => DBT m a -> m (Either ErrorMsg a)
runDb action = do 
      dbName <- liftIO $ getDbName
      runDBT dbName action

-- |SQL Statement to create the nodes table.
createNodeTable :: String
createNodeTable = "CREATE TABLE IF NOT EXISTS nodes "++
                  " (id INTEGER PRIMARY KEY AUTOINCREMENT,"++
                  "  node_type CHAR(2),"++
                  "  node_value_1 VARCHAR,"++
                  "  node_value_2 VARCHAR,"++
                  " UNIQUE (node_type, node_value_1, node_value_2))"

-- |SQL Statement to create the triples table.
createTripleTable :: String
createTripleTable = "CREATE TABLE IF NOT EXISTS triples "++
                    " (subject INTEGER,"++  
                    "  predicate INTEGER,"++
                    "  object INTEGER,"++
                    "  FOREIGN KEY (subject)   REFERENCES nodes (id),"++
                    "  FOREIGN KEY (predicate) REFERENCES nodes (id),"++
                    "  FOREIGN KEY (object)    REFERENCES nodes (id),"++
                    " UNIQUE (subject, predicate, object));"

-- |Set up a new database.
createDb :: DbOp ()
createDb = do _ <- run' createNodeTable
              _ <- run' createTripleTable
              commit
              return ()

-- |Drop all the contents of our database.
dropDb :: DbOp ()
dropDb = do mapM_ run' ["DROP TABLE IF EXISTS triples",
                        "DROP TABLE IF EXISTS nodes"]
            commit
            return ()

-- |Reset our database, re-creating tables.
resetDb :: DbOp ()
resetDb = dropDb >> createDb

-- |The SQL values necessary to represent a node. Length must be three.
type SqlNode = [SqlValue]

-- |The SQL values necessary to represent a node. Length must be three and each sublist must also be of length three.
type SqlNodes = [SqlNode]

-- |All the nodes to represent a list of triples. length is arbitrary, but sublists have restrictions like 'SqlNodes'
type SqlTriplesNodes = [SqlNodes]

insertTriplesStmt :: String
insertTriplesStmt = "INSERT OR IGNORE "++
                   " INTO triples (subject, "++
                                  "predicate, "++
                                  "object)"++
                   " VALUES (?, ?, ?)"

insertNodesStmt :: String
insertNodesStmt = "INSERT OR IGNORE "++
                 " INTO nodes (node_type, node_value_1, node_value_2) "++
                 " VALUES (?, ?, ?)"

selNodesStmt :: String
selNodesStmt = "SELECT id FROM nodes "++
              " WHERE node_type = ? "++
              " AND node_value_1 = ? "++
              " AND node_value_2 = ?"

-- |'insertNodes nodes' takes the 'SqlValue' representation of a single triple and returns three integer 'SqlValues' corresponding to the ids of the newly inserted nodes. 
-- If some of the nodes already exist in the database, ids of these nodes are returned.
insertNodes :: SqlNodes -> DbOp [SqlValue]
insertNodes nodes = do 
              s1 <- prepare insertNodesStmt
              s2 <- prepare selNodesStmt 
              insertNodes' s1 s2 nodes

-- insertNodes and insertTriplesP helper
insertNodes' :: Statement -> Statement -> SqlNodes -> DbOp [SqlValue]
insertNodes' _  _  []       = return []
insertNodes' s1 s2 (n :ns)  = do _ <- execute s1 n
                                 _ <- execute s2 n
                                 r <- fetchRow s2
                                 case r of
                                      Just [rowid] -> (rowid:) <$> insertNodes' s1 s2 ns
                                      _ -> error "unexpected result insertNodes"

-- insertTriples and insertTriplesP helper
insertTriples' :: (Convertible a SqlValue) => Statement -> [[a]] -> DbOp ()
insertTriples' stmt triples = do
              executeMany stmt ((map.map $ toSql) triples)

-- insertTriples and insertTriplesP helper
insertTriple' :: (Convertible a SqlValue) => Statement -> [a] -> DbOp Integer
insertTriple' stmt triples = do
              _ <- execute stmt (map toSql triples)
              return 0

-- |'insertTriplesP' inserts triples via a pipe. 'executeMany' is not used,
-- but with Sqlite it doesn't seem to be optimized anyway.
insertTriplesP :: (MonadIO m, Functor m) => Consumer Triple (DBT m) r
insertTriplesP = do
              insTrip <- lift $ prepare insertTriplesStmt
              insNode <- lift $ prepare insertNodesStmt
              selNode <- lift $ prepare selNodesStmt                                   
              forever $ do t <- await
                           let nodes = tripleToSqlValues t
                           nodeIds <- lift $ insertNodes' insNode selNode nodes
                           lift $ insertTriple' insTrip nodeIds

-- |'insertTriples triples' inserts a list of triples into the database
insertTriples :: Triples -> DbOp ()
insertTriples triples = do 
              insTrip <- prepare insertTriplesStmt
              insNode <- prepare insertNodesStmt
              selNode <- prepare selNodesStmt
              let (nodes :: SqlTriplesNodes) = map tripleToSqlValues triples
              nodeIds <- mapM (insertNodes' insNode selNode) nodes
              insertTriples' insTrip nodeIds 

-- |Insert a triple
insertTriple :: Triple -> DbOp ()
insertTriple t = insertTriples [t]

tripleToSqlValues :: Triple -> SqlNodes
tripleToSqlValues (Triple n1 n2 n3) = map nodeToSqlValue [n1, n2, n3]

nodeToSqlValue :: Node -> [SqlValue]
nodeToSqlValue (UNode text)                  = [toSql "U",  toSql text,       toSql ""]
nodeToSqlValue (BNode text)                  = [toSql "B",  toSql text,       toSql ""]
nodeToSqlValue (BNodeGen int)                = [toSql "BG", toSql (show int), toSql ""]
nodeToSqlValue (LNode (PlainL text))         = [toSql "L",  toSql text,       toSql ""]
nodeToSqlValue (LNode (PlainLL text1 text2)) = [toSql "LL", toSql text1,      toSql text2]
nodeToSqlValue (LNode (TypedL text1 text2))  = [toSql "TL", toSql text1,      toSql text2]

nodeFromSqlValues :: SqlValue -> SqlValue -> SqlValue -> DbOp Node
nodeFromSqlValues typ val1 val2 = 
                  let typ' = (fromSql typ :: String)
                  in case typ' of
                     "U"  -> return . UNode . fromSql $ val1
                     "B"  -> return . BNode . fromSql $ val1
                     "BG" -> return . BNodeGen . fromSql $ val1
                     "L"  -> return . LNode . PlainL . fromSql $ val1
                     "LL" -> return . LNode $ PlainLL (fromSql val1) (fromSql val2)
                     "TL" -> return . LNode $ TypedL (fromSql val1) (fromSql val2)
                     t    -> throwError . strMsg $ "nodeFromSqlValues: Unexpected type "++t

tripleFromSqlValues :: (SqlValue, SqlValue, SqlValue) -> 
                       (SqlValue, SqlValue, SqlValue) -> 
                       (SqlValue, SqlValue, SqlValue) -> 
                       DbOp Triple
tripleFromSqlValues n1 n2 n3 = do
                    n1' <- node n1
                    n2' <- node n2
                    n3' <- node n3
                    return $ Triple n1' n2' n3'
                  where node (a,b,c) = nodeFromSqlValues a b c

tripleFromSqlValues' :: [SqlValue] -> DbOp Triple
tripleFromSqlValues' svs = case svs of 
                                [a,b,c,d,e,f,g,h,i] -> tripleFromSqlValues (a,b,c) (d,e,f) (g,h,i)
                                _ -> throwError . strMsg $ 
                                     "tripleFromSqlValues': unexpected number of values: " 
                                      ++ show (length svs)

triplesByStmt :: String
triplesByStmt = "SELECT n1.node_type, n1.node_value_1, n1.node_value_2, "++
                "       n2.node_type, n2.node_value_1, n2.node_value_2, "++
                "       n3.node_type, n3.node_value_1, n3.node_value_2 "++
                "FROM   triples t, nodes n1, nodes n2, nodes n3  "++
                "WHERE  n1.id = t.subject "++
                "AND    n2.id = t.predicate "++
                "AND    n3.id = t.object "

fixNStmt :: String -> String
fixNStmt n = "AND "++n++".node_type   = ? " ++
             "AND "++n++".node_value_1 = ? "++
             "AND "++n++".node_value_2 = ? "

fixSubjectStmt, fixPredicateStmt, fixObjectStmt :: String
fixSubjectStmt = fixNStmt "n1"
fixPredicateStmt = fixNStmt "n2"
fixObjectStmt = fixNStmt "n3"

triplesBy :: [String] -> [Node] -> DbOp Triples
triplesBy stmts nodes = do
      let sqlNodes = concat . map nodeToSqlValue $ nodes
      let stmt = triplesByStmt ++ intercalate " " stmts
      queryRes <- quickQuery stmt sqlNodes
      mapM tripleFromSqlValues' queryRes

-- | Read all triples as a list.
readTriples :: DbOp Triples
readTriples = triplesBy [] []

-- | Read all triples in a pipe.
readTriplesP :: (MonadIO m, Functor m) => Producer Triple (DBT m) Int
readTriplesP = do stmt <- lift $ prepare triplesByStmt
                  _ <- lift $ execute stmt []
                  go 0 stmt
         where go n stmt = do row <- lift $ fetchRow stmt
                              case row of
                                 Nothing -> return n 
                                 Just r -> do t <- lift $ tripleFromSqlValues' r
                                              yield t
                                              go (n+1) stmt

-- | Select triples by subject.
triplesBySubject :: Node -> DbOp Triples
triplesBySubject subject = triplesBy [fixSubjectStmt] [subject]

-- | Select triples by predicate.
triplesByPredicate :: Node -> DbOp Triples
triplesByPredicate predicate = triplesBy [fixPredicateStmt] [predicate]

-- | Select triples by object.
triplesByObject :: Node -> DbOp Triples
triplesByObject object = triplesBy [fixObjectStmt] [object]

-- | Select triples by subject and predicate.
triplesBySubjectAndPredicate :: Node -> Node -> DbOp Triples
triplesBySubjectAndPredicate subject predicate = triplesBy [fixSubjectStmt, fixPredicateStmt] [subject, predicate]

-- | Select triples by predicate and object.
triplesByPredicateAndObject :: Node -> Node -> DbOp Triples
triplesByPredicateAndObject predicate object = triplesBy [fixPredicateStmt, fixObjectStmt] [predicate, object]

-- | Select triples by subject and object.
triplesBySubjectAndObject :: Node -> Node -> DbOp Triples
triplesBySubjectAndObject subject object = triplesBy [fixSubjectStmt, fixObjectStmt] [subject, object]

