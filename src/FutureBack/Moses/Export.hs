{- |
Module      : FutureBack.Moses.Export
Copyright   : (c) Michael Hartl 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : unix

Main function for the export utility
-}

module FutureBack.Moses.Export (runExport) where

import Pipes
import FutureBack.Common
import FutureBack.Moses.Store
import Data.RDF
import Data.RDF.TriplesGraph
import Data.Either
import Data.Text (Text)
import qualified Data.Text as T
import Control.Exception (bracket)
import System.IO

myBaseUrl = Nothing
mySelfRef = Nothing

-- | export the database contents in NTriples format
runExport
    :: FilePath -- ^File path to store exported file in
    -> IO (Either ErrorMsg Int)
runExport fp = bracket (openFile fp WriteMode) (hClose) (\h ->
      runDb . runEffect $ for readTriplesP $ liftIO . hWriteT NTriplesSerializer h)

