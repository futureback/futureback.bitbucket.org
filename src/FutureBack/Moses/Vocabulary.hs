{- |
Module      : FutureBack.Moses.Vocabulary
Copyright   : (c) Michael Hartl, Martin Stücklschwaiger 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : portable

RDF vocabulary used by futureBack.
-}

module FutureBack.Moses.Vocabulary (
       module FutureBack.Moses.Vocabulary.Files,
       module FutureBack.Moses.Vocabulary.Backup) where

import FutureBack.Moses.Vocabulary.Files
import FutureBack.Moses.Vocabulary.Backup
