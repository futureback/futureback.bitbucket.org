{-# LANGUAGE OverloadedStrings, RankNTypes #-}

{- |
Module      : FutureBack.Moses.Vocabulary.Backup
Copyright   : (c) Martin Stücklschwaiger, Michael Hartl, 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : unix

Triple-Store for file-related data.
-}
    
module FutureBack.Moses.FileStore
    ( getFiles
    , getDirectories
    , getFilesAndDirectories
    , infoToInfoHash
    , saveFileInfoHash
    ) where

import Data.RDF
import FutureBack.Moses.Store
import FutureBack.Moses.Vocabulary
import FutureBack.Common
import System.Posix.Files.ByteString
import qualified Data.Text as T
import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString.Lazy.Char8 as LB
import qualified Data.Maybe as M
import Data.List
import Data.Function
import Control.Applicative ((<$>))
import Data.Digest.Pure.MD5 (md5, hash)
import System.IO

-- | Compute the hash for a file
-- TODO: better do a lookup in the Database here :) 
infoToInfoHash
    :: RawFilePath -- ^the base directory being backed up
    -> FileInfo    -- ^the file for which we want a hash
    -> DbOp FileInfoHash
infoToInfoHash source (fp,fstat) = do
  hashNeeded <- isHashNeeded (fp, fstat)
  mHash <- if hashNeeded
    then do
      fileHash <- liftIO (computeHash (source // fp) :: IO NiceHash)
      return (Just fileHash)
    else
      return Nothing

  return (fp, fstat, mHash)


-- | Saves everything about the file into the DB
saveFileInfoHash 
    :: Backup       -- ^the currently running backup
    -> FileInfoHash -- ^the file data to be saved
    -> DbOp ()
saveFileInfoHash backup fih@(fp, fstat, hash) = do
  case (hash, fileTypeFromFileStatus fstat) of
    (Nothing, RegularFile) -> liftIO . hPutStrLn stderr $ "Warning: Could not get hash from " ++
                                                          show fp ++ ", ignoring."
    (Just _,  RegularFile) -> save backup fih
    (Nothing, Directory)   -> save backup fih
    (_, fileType)          -> liftIO . hPutStrLn stderr $ "Warning: " ++ show fp ++
                                                          "is of currently unsupported type "++
                                                          show fileType++", ignoring."
  where
    save :: Backup -> FileInfoHash -> DbOp ()
    save backup fih = do
      let triples = createTriples backup fih
      insertTriples triples
      return ()

-- | Just a record to ensure all nodes about the file are set 
--   and to nicely access them
data FileNodes = FileNodes 
  { filePathNode     :: Node
  , fileMetaNode     :: Node
  , fileModifiedNode :: Node
  , fileTypeNode     :: Node
  , mFileHashNode    :: Maybe Node
  } deriving (Eq, Show)

-- | Creates a list of triples for a file and a given Backup
createTriples :: Backup -> FileInfoHash -> Triples
createTriples backup info@(_, _, mHash) = case mHash of
    Just fileHash -> (hashTriple fileHash) : triples
    Nothing       -> triples
  where
    nodes        = fileInfoHashToFileNodes info
    fileNode     = BNode $ nodeHash nodes
    triples      = backupTriple : (nodesToTriples fileNode nodes)
    hashTriple h = Triple fileNode fbHash (fileHashToNode h)
    backupTriple = Triple backupNode fbHasFile fileNode
    backupNode   = backupToNode backup
    nodeHash :: Show a => a -> T.Text
    nodeHash = md5HashToNiceHash . md5 . LB.pack . show


nodesToTriples :: Subject -> FileNodes -> Triples
nodesToTriples fileNode nodes = 
    [ Triple fileNode fbPath     (filePathNode nodes)
    , Triple fileNode fbMeta     (fileMetaNode nodes)
    , Triple fileNode fbModified (fileModifiedNode nodes)
    , Triple fileNode fbType     (fileTypeNode nodes)
    ]


fileInfoHashToFileNodes :: FileInfoHash -> FileNodes
fileInfoHashToFileNodes (fp, fstat, mHash) = FileNodes 
  { filePathNode     = filePathToNode fp
  , fileMetaNode     = fileMetaToNode $ fileMetaFromFileStatus fstat
  , fileModifiedNode = fileDateToNode $ modificationTime fstat
  , fileTypeNode     = fileTypeToNode . fileTypeFromFileStatus $ fstat
  , mFileHashNode    = liftM fileHashToNode mHash
  }

-- | Checks, whether a Hash generation is even needed.
--   TODO: needs to be further improved or refactored
isHashNeeded :: FileInfo -> DbOp Bool
isHashNeeded (_,fstat) = do
    if (fileTypeFromFileStatus fstat == RegularFile)
      then return True
      else return False

-- | Calculates a NiceHash for a given File
computeHash :: RawFilePath -> IO NiceHash
computeHash fp = liftM (md5HashToNiceHash . hash) (LB.readFile . B.unpack $ fp)

-- | Generates a list of file nodes for a given BackupId
getFileNodes :: BackupId -> DbOp [Node]
getFileNodes bId = do
    let backupNode = LNode . PlainL $ bId
    triples <- triplesBySubjectAndPredicate backupNode fbHasFile
    return $ map objectOf triples

-- | Just nicer than to write the long version.
--   FileData (niceHash, originalPath, fileMeta)
type FileData = (Maybe T.Text, RawFilePath, FileMeta)

-- | Generates a list of files for a given BackupId. 
--   No directories are included
getFiles :: BackupId -> DbOp [FileData]
getFiles fd = fst . splitFileDirectories <$> getFilesAndDirectories fd

-- | Generates a list of directories for a given BackupId. 
getDirectories :: BackupId -> DbOp [FileData]
getDirectories fd = sortBy (compare `on` getSnd) . 
                    snd . 
                    splitFileDirectories <$> getFilesAndDirectories fd
                  where
                    getSnd (_, x, _) = x

-- | Splits a list of files & directories into (File-List, Directory-List)
splitFileDirectories :: [FileData] -> ([FileData], [FileData])
splitFileDirectories = partition (\(_, _, fm) -> metaFileType fm /= Directory)


-- | Generates a list of files & directories for a given BackupId.
getFilesAndDirectories :: BackupId -> DbOp [FileData]
getFilesAndDirectories bId = do
  fileNodes <- getFileNodes bId
  fileDatas <- mapM nodeToFileData fileNodes
  return $ M.catMaybes fileDatas

-- | Generates FileData for a given Node
nodeToFileData :: Node -> DbOp (Maybe FileData)
nodeToFileData fileNode = do
  mHash <- fileNodeToMaybeHash fileNode
  mPath <- fileNodeToMaybePath fileNode
  mMeta <- fileNodeToMaybeMeta fileNode
  let info = (mHash, mPath, mMeta)
  return $ case info of
    (hash, Just path, Just meta) -> Just (hash, path, meta)
    _                                 -> Nothing

fileNodeToMaybeMeta :: Node -> DbOp (Maybe FileMeta)
fileNodeToMaybeMeta fileNode = do
  triples <- triplesBySubjectAndPredicate fileNode fbMeta
  let meMeta = map (nodeToFileMeta . objectOf) triples
  return $ case meMeta of
      (Right meta:_) -> Just meta
      _              -> Nothing

fileNodeToMaybeHash :: Node -> DbOp (Maybe NiceHash)
fileNodeToMaybeHash fileNode = do
    triples <- triplesBySubjectAndPredicate fileNode fbHash
    let meHash = map (nodeToNiceHash . objectOf) triples
    return $ case meHash of 
        (Right hash:_) -> Just hash
        _              -> Nothing

fileNodeToMaybePath :: Node -> DbOp (Maybe RawFilePath)
fileNodeToMaybePath fileNode = do
    triples <- triplesBySubjectAndPredicate fileNode fbPath
    let mePath = map (nodeToFilePath . objectOf) triples
    return $ case mePath of
        (Right path:_) -> Just path
        _              -> Nothing
