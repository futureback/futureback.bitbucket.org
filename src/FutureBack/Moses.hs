module FutureBack.Moses where

{- |

Module      : FutureBack.Paul
Copyright   : (c) Martin Stücklschwaiger, Michael Hartl, 2014
Maintainer  : mikehartl17@gmail.com
Stability   : experimental
Portability : unix

Moses is the law-gui in the Old Testament, and FutureBack.Moses is
the backend, keeping everything in place where it belongs.

-}
