-- |
-- Module      : FutureBack.Paul
-- Copyright   : (c) Martin Stücklschwaiger, Michael Hartl, 2014
-- Maintainer  : mikehartl17@gmail.com
-- Stability   : experimental
-- Portability : unix
-- 
-- Paul in the New Testament is the messenger of the gospel
-- which governs the relationship between God and men. 
--
-- FutureBack.Paul is responsible for communicating between
-- the filesystem and the database.
--
-- For now, this is the main executable which is compiled into
-- 'futureBack'.

module FutureBack.Paul (run) where

import FutureBack.Paul.Backup
import FutureBack.Paul.Restore
import FutureBack.Common
import FutureBack.Moses
import FutureBack.Moses.BackupStore
import FutureBack.Moses.Store hiding (run)

import System.Console.GetOpt
import Control.Monad
import Data.List
import Data.Char
import System.IO
import System.Exit
import System.Environment
import Data.Maybe
import qualified Data.ByteString.Char8 as B
import qualified Data.Text as T
import System.Time.Utils (epochToClockTime)

{- | Command line option flags -}
data Flag
    = Version
    | Help
    | Backup
    | Restore
    | List
    | Local String
    | Remote String
    | BackupId String
    deriving (Read, Show, Eq)

{- | Command line options structure -}
data Options = Options
    { optBackup   :: Bool
    , optRestore  :: Bool
    , optLocal    :: Maybe RawFilePath
    , optRemote   :: Maybe RawFilePath
    , optBackupId :: Maybe T.Text
    } deriving (Read, Show, Eq)

-- |
-- option handling is totally copied from the haskell wiki:
-- http://www.haskell.org/haskellwiki/High-level_option_handling_with_GetOpt
options :: String -> [OptDescr (Options -> IO Options)]
options progName = 
          [ Option "b" ["backup"]
                   (NoArg (\opt -> return opt { optBackup = True }))
                   "Run in backup mode (default)"
          , Option "R" ["restore"]
                   (NoArg (\opt -> return opt { optRestore = True }))
                   "Run in restore mode"
          , Option "l" ["local"]
                   (ReqArg (\arg opt -> return opt { optLocal = Just . B.pack $ arg })
                           "DIR_OR_FILE")
                   "Local directory or file"
          , Option "r" ["remote"]
                   (ReqArg (\arg opt -> return opt { optRemote = Just . B.pack $ arg })
                           "TARGET")
                   "Remote directory"
          , Option "i" ["id"]
                   (ReqArg (\arg opt -> return opt { optBackupId = Just . T.pack $ arg })
                           "BACKUP_ID")
                   "Backup ID"
          , Option "v" ["version"]
                   (NoArg (\_ -> do hPutStrLn stderr fbVersion
                                    exitWith ExitSuccess))                                  
                   "Display program version"
          , Option "L" ["list"]
                   (NoArg (\_ -> do runDb printBackupIds
                                    exitWith ExitSuccess))
                   "List all available backups"
          , Option "c" ["clean"]
                   (NoArg (\_ -> do runDb resetDb
                                    exitWith ExitSuccess))
                   "Reset the database"
          , Option "h" ["help"]
                   (NoArg (\_ -> do hPutStrLn stderr (usageInfo progName (options progName))
                                    exitWith ExitSuccess))
                   "Print this help message"
          ]

-- | do some basic option validations
validateOptions :: Options -> IO Options
validateOptions opts = do
     let Options { optBackup   = backup
                 , optRestore  = restore
                 , optLocal    = local
                 , optRemote   = remote
                 , optBackupId = backupId
                 } = opts
     when (backup && restore) $ error "-b (backup) and -R (restore) are mutually exclusive"
     when (local  == Nothing) $ error "must specify -l (local) option at command line"
     -- TODO read remote from backup store if not set
     when (remote == Nothing) $ error "must specify -r (remote) option at command line"
     when (restore && backupId == Nothing) $ error "-i (id) is required for restore"
     return $ Options { optBackup   = backup || not restore
                      , optRestore  = restore
                      , optLocal    = local
                      , optRemote   = remote
                      , optBackupId = backupId
                      }

-- | run the program
run :: String   -- ^program name
    -> [String] -- ^command line arguments
    -> IO ()
run progName args = do
     let (actions, nonOptions, errors) = getOpt RequireOrder (options progName) args
     let startOptions = Options
                        { optBackup   = False
                        , optRestore  = False
                        , optLocal    = Nothing
                        , optRemote   = Nothing
                        , optBackupId = Nothing
                        }
     opts <- foldl (>>=) (return startOptions) actions
     opts' <- validateOptions opts

     let operation = if   optBackup opts'
                     then backup  (fromJust $ optLocal opts) (fromJust $ optRemote opts)
                     else restore
                       (fromJust $ optLocal opts)
                       (fromJust $ optRemote opts)
                       (fromJust $ optBackupId opts)

     res <- runDb (createDb >> operation)
     case res of
          Left err -> print err
          Right () -> return ()

printBackupIds :: (MonadIO m, Functor m) => DBT m ()
printBackupIds = do
    backupDates <- getBackupTimes
    liftIO $ printBackupDates backupDates
    return ()

printBackupDates :: [(T.Text, Maybe EpochTime)] -> IO ()
printBackupDates backups = do
  putStrLn "Backups: "
  let strs = map (\(b,t) -> T.unpack b ++ " - " ++ mETimeToStr t) backups
  putStrLn . unlines $ strs
  return ()


mETimeToStr :: Maybe EpochTime -> String
mETimeToStr Nothing = "-"
mETimeToStr (Just time) = show clockTime
  where
    clockTime = epochToClockTime time
