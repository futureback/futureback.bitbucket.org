module Test.FutureBack.DirWalk (run) where

import FutureBack.Paul.DirWalk
import FutureBack.Common
import Pipes
import System.IO
import qualified Data.ByteString.Char8 as B



withoutHash :: String -> Proxy X () a b FBIO ()
withoutHash root = for (findFiles $ B.pack root) (lift . \itm -> do
                     case itm of
                          Left err -> liftIO $ B.hPutStrLn stderr (B.pack . show $ err)
                          Right (fp, _) -> liftIO $ B.putStrLn $ fp)


run :: String -> IO ()
run dir = do res <- runFBT $ runEffect (withoutHash dir)
             case res of
                  Left err -> print err
                  Right res' -> print "ok"
