{-# LANGUAGE OverloadedStrings #-}

module Test.FutureBack.DB (run) where

import FutureBack.Common
import FutureBack.Moses.Store hiding (run)
import FutureBack.Moses.FileStore
import FutureBack.Moses.BackupStore
import FutureBack.Paul.DirWalk
import Pipes
import qualified Data.Text as T
import System.Time.Utils (epochToClockTime)

insertFiles backup source = for (findFiles source) $ lift . eSave
    where 
      eSave (Right file@(fp,_)) = do 
          fih <- infoToInfoHash source file
          saveFileInfoHash backup fih
          --liftIO $ print fp
          return ()
      eSave _ = return ()

showFiles source = for (findFiles source) $ lift . run'
    where run' (Right (fp,_)) = liftIO $ print $ fp
          run' _              = return ()


run :: IO ()
run = do 
  res <- runDb $ do
    resetDb

    runBackup "files" "dest"
    liftIO $ putStrLn "---"

    liftIO $ putStrLn "Backups: "

    backupDates <- getBackupTimes
    liftIO $ printBackupDates backupDates

    let backupId = fst . head $ backupDates


    files <- getFiles backupId

    liftIO $ mapM_ print files

    --ts <- readTriples
    --liftIO $ mapM_ print ts
    return ()
  case res of 
    Left err -> print err
    Right () -> putStrLn "OK."


runBackup :: (Functor m, MonadIO m) => RawFilePath -> RawFilePath -> DBT m ()
runBackup source destination = do
  backup <- liftIO $ createBackup source destination
  saveBackupStarted backup
  runEffect $ showFiles source
  runEffect $ insertFiles backup source
  saveBackupDone backup
  return ()


printBackupDates :: [(T.Text, Maybe EpochTime)] -> IO ()
printBackupDates backups = do
  putStrLn "Backups: "
  let strs = map (\(b,t) -> T.unpack b ++ " - " ++ mETimeToStr t) backups
  putStrLn . unlines $ strs
  return ()


mETimeToStr :: Maybe EpochTime -> String
mETimeToStr Nothing = "-"
mETimeToStr (Just time) = show clockTime
  where
    clockTime = epochToClockTime time
