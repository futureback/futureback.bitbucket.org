{-# LANGUAGE OverloadedStrings #-}
module Test.FutureBack.Operations (run) where

import FutureBack.Common
import FutureBack.Moses.Operations
import FutureBack.Paul.DirWalk
import qualified Data.Text as T
import qualified Data.Text.Encoding as E
import qualified Data.ByteString.Char8 as B
import System.Posix.Files.ByteString
import Pipes
import Text.Printf

targetName :: RawFilePath -> T.Text
targetName fp = E.decodeUtf8 $ baseName fp |+ ".target"

targetDir :: RawFilePath
targetDir = "/tmp/backup"

run :: RawFilePath -> IO ()
run root = do
    res <- runFBT $ do runHandlerT' $ ensureDirectory targetDir Nothing
                       runHandlerT' $ runEffect loop
    case res of
         Left err -> print err
         Right _ -> print ("ok" :: String)

    where loop :: Effect (HandlerT FBIO) ()
          loop = for (findFiles root) $ lift . process

          process :: Either ErrorMsg FileInfo -> HandlerT FBIO ()
          process rec = do
               case rec of
                    Left err -> liftIO $ print err
                    Right (fp, fstat) ->
                          when (fstat `isFileType` RegularFile) $ do
                               backupFileAs fp (targetName fp) targetDir
                               liftIO . putStrLn $ printf "backup %s as %s"
                                                          (B.unpack fp)
                                                          (T.unpack $ targetName fp)
                               restoreFile (targetDir /+ targetName fp) fp (fileMode fstat) (fromIntegral 0)