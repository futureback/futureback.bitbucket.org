
futureBack
==========
von Mike Hartl und Martin Stücklschwaiger


Doku
----
[futureback.bitbucket.org/doc](http://futureback.bitbucket.org/doc)


Aufgabenstellung
----------------
Wir bauen mittels Pipes, RDF4H und SQLite eine Commandline Backuplösung, wobei wir die Informationen über die vorhandenen Dateien in RDF / SQLite ablegen und die Daten selbst ohne Filehierachie speichern. Dateien selbst werden dabei mittels MD5-Hash eindeutig identifiziert und Duplikate dadurch vermieden. 


User Interface
--------------
Geplant ist ein Command Line Interface, das vorerst die folgenden Befehle unterstützten sollte:

* futureBack backup <folder> <backup-folder>
  Creates a backup of the given directory

* futureBack restore <backup-version> <backup-folder> <folder>
  Restores a backup in the given directory

* futureBack list <folder>
  Lists all available backups for the given directory


Für später geplant
------------------
* GUI
* Zusätzliche Backupspeichermedien
* Kontinuierliche Backups mittels Hintergrundprozess

RDF Nodestruktur
----------------
![Nodestruktur.png](https://bitbucket.org/repo/kR6RL8/images/2129185430-Nodestruktur.png)

Wurde mittels [draw.io](http://draw.io) erstellt und lässt sich dort auch öffnen und weiterverarbeiten (Source ist im PNG embedded). 



---
<link href="http://jasonm23.github.io/markdown-css-themes/foghorn.css" rel="stylesheet">
