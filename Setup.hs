import Distribution.Simple
import Distribution.Simple.UserHooks
import Distribution.Simple.Setup
import Distribution.PackageDescription
import Distribution.Simple.LocalBuildInfo
import System.Cmd
import System.Exit


main = 
  defaultMainWithHooks simpleUserHooks
  { postHaddock = ph
  }


ph :: Args -> HaddockFlags -> PackageDescription -> LocalBuildInfo -> IO ()  
ph _ _ _ _ = 
  copyDir "dist/doc/html/futureBack/" "doc" >> return ()


copyDir ::  FilePath -> FilePath -> IO (ExitCode)
copyDir src dest = system $ "cp -r " ++ src ++ " " ++ dest
